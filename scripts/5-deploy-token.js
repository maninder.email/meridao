import sdk from "./1-initialize-sdk.js";

// In order to deploy the new contract we need our old friend the app module again.
const app = sdk.getAppModule("0xda04460CD5264fb4aa2b972fE12CCde6f60e8980");

(async () => {
  try {
    // Deploy a standard ERC-20 contract.
    const tokenModule = await app.deployTokenModule({
      // What's your token's name? Ex. "Ethereum"
      name: "meriDAO Governance Token",
      // What's your token's symbol? Ex. "ETH"
      symbol: "meriDAO",
    });
    console.log(
      "✅ Successfully deployed token module, address:",
      tokenModule.address
    );
  } catch (error) {
    console.error("failed to deploy token module", error);
  }
})();

// output
// (node:23314) ExperimentalWarning: buffer.Blob is an experimental feature. This feature could change at any time
// (Use `node --trace-warnings ...` to show where the warning was created)
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// ✅ Successfully deployed token module, address: 0x0433dFb5dbc2AcA2e30ffCcd9213A0e78A386dED
