import sdk from "./1-initialize-sdk.js";
import { readFileSync } from "fs";

const bundleDrop = sdk.getBundleDropModule(
  "0x3Bb56c17C568d79F964556bE5287fCd96AB9617b"
);

(async () => {
  try {
    await bundleDrop.createBatch([
      {
        name: "Circle of Life",
        description: "This NFT will give you access to Circle of Life",
        image: readFileSync("scripts/assets/circle.jpg"),
      },
    ]);
    console.log("✅ Successfully created a new NFT in the drop!");
  } catch (error) {
    console.error("failed to create the new NFT", error);
  }
})();


// output of the above script is :
// (node:20952) ExperimentalWarning: buffer.Blob is an experimental feature. This feature could change at any time
// (Use `node --trace-warnings ...` to show where the warning was created)
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// ✅ Successfully created a new NFT in the drop!