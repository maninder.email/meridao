import sdk from "./1-initialize-sdk.js";

// Grab the app module address.
const appModule = sdk.getAppModule(
  "0xda04460CD5264fb4aa2b972fE12CCde6f60e8980"
);

(async () => {
  try {
    const voteModule = await appModule.deployVoteModule({
      // Give your governance contract a name.
      name: "meriDAO's Epic Proposals",

      // This is the location of our governance token, our ERC-20 contract!
      votingTokenAddress: "0x0433dFb5dbc2AcA2e30ffCcd9213A0e78A386dED",

      // After a proposal is created, when can members start voting?
      // For now, we set this to immediately.
      proposalStartWaitTimeInSeconds: 0,

      // How long do members have to vote on a proposal when it's created?
      // Here, we set it to 24 hours (86400 seconds)
      proposalVotingTimeInSeconds: 24 * 60 * 60,

      // Will explain more below.
      votingQuorumFraction: 0,

      // What's the minimum # of tokens a user needs to be allowed to create a proposal?
      // I set it to 0. Meaning no tokens are required for a user to be allowed to
      // create a proposal.
      minimumNumberOfTokensNeededToPropose: "0",
    });

    console.log(
      "✅ Successfully deployed vote module, address:",
      voteModule.address
    );
  } catch (err) {
    console.log("Failed to deploy vote module", err);
  }
})();

// output :
// (node:25506) ExperimentalWarning: buffer.Blob is an experimental feature. This feature could change at any time
// (Use `node --trace-warnings ...` to show where the warning was created)
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// ✅ Successfully deployed vote module, address: 0x993d74f0EE94F9521821A302337F6Acc2A99baB2
