import sdk from "./1-initialize-sdk.js";

const bundleDrop = sdk.getBundleDropModule(
  "0x3Bb56c17C568d79F964556bE5287fCd96AB9617b"
);

(async () => {
  try {
    const claimConditionFactory = bundleDrop.getClaimConditionFactory();
    // Specify conditions.
    claimConditionFactory.newClaimPhase({
      startTime: new Date(),
      maxQuantity: 50_000,
      maxQuantityPerTransaction: 1,
    });

    await bundleDrop.setClaimCondition(0, claimConditionFactory);
    console.log("✅ Sucessfully set claim condition!");
  } catch (error) {
    console.error("Failed to set claim condition", error);
  }
})();

// output of the above script is :
// (node:21129) ExperimentalWarning: buffer.Blob is an experimental feature. This feature could change at any time
// (Use `node --trace-warnings ...` to show where the warning was created)
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// ✅ Sucessfully set claim condition!
