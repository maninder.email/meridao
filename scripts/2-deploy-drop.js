import { ethers } from "ethers";
import sdk from "./1-initialize-sdk.js";
import { readFileSync } from "fs";

const app = sdk.getAppModule("0xda04460CD5264fb4aa2b972fE12CCde6f60e8980");

(async () => {
  try {
    const bundleDropModule = await app.deployBundleDropModule({
      name: "MYDAO",
      description: "first for MYDAO",
      image: readFileSync("scripts/assets/logo.jpeg"),
      primarySaleRecipientAddress: ethers.constants.AddressZero,
    });

    console.log(
      "✅ Successfully deployed bundleDrop module, address:",
      bundleDropModule.address
    );
    console.log(
      "✅ bundleDrop metadata:",
      await bundleDropModule.getMetadata()
    );
  } catch (error) {
    console.log("failed to deploy bundleDrop module", error);
  }
})();


// output of the above script is :
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// ✅ Successfully deployed bundleDrop module, address: 0x3Bb56c17C568d79F964556bE5287fCd96AB9617b
// ✅ bundleDrop metadata: {
//   metadata: {
//     name: 'MYDAO',
//     description: 'first for MYDAO',
//     image: 'https://cloudflare-ipfs.com/ipfs/bafybeihfeznx3uf3ejbvslehgfk2j3manrezhwqafs2w544twhdsifij5y',
//     primary_sale_recipient_address: '0x0000000000000000000000000000000000000000',
//     uri: 'ipfs://bafkreiadltaao37r6hblm2didses7zrfvgjkwgiaijydeomtiu4bvmbnni'
//   },
//   address: '0x3Bb56c17C568d79F964556bE5287fCd96AB9617b',
//   type: 11
// }
