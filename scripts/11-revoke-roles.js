import sdk from "./1-initialize-sdk.js";

const tokenModule = sdk.getTokenModule(
  "0x0433dFb5dbc2AcA2e30ffCcd9213A0e78A386dED",
);

(async () => {
  try {
    // Log the current roles.
    console.log(
      "👀 Roles that exist right now:",
      await tokenModule.getAllRoleMembers()
    );

    // Revoke all the superpowers your wallet had over the ERC-20 contract.
    await tokenModule.revokeAllRolesFromAddress(process.env.WALLET_ADDRESS);
    console.log(
      "🎉 Roles after revoking ourselves",
      await tokenModule.getAllRoleMembers()
    );
    console.log("✅ Successfully revoked our superpowers from the ERC-20 contract");

  } catch (error) {
    console.error("Failed to revoke ourselves from the DAO treasury", error);
  }
})();

// output :
// node scripts/11-revoke-roles.js
// (node:26758) ExperimentalWarning: buffer.Blob is an experimental feature. This feature could change at any time
// (Use `node --trace-warnings ...` to show where the warning was created)
// Your app address is: 0xda04460CD5264fb4aa2b972fE12CCde6f60e8980
// 👀 Roles that exist right now: {
//   admin: [ '0xfCa05007b8B0495C5a8E219059e519510026067c' ],
//   minter: [
//     '0xfCa05007b8B0495C5a8E219059e519510026067c',
//     '0x993d74f0EE94F9521821A302337F6Acc2A99baB2'
//   ],
//   pauser: [ '0xfCa05007b8B0495C5a8E219059e519510026067c' ],
//   transfer: [ '0xfCa05007b8B0495C5a8E219059e519510026067c' ]
// }
// 🎉 Roles after revoking ourselves {
//   admin: [],
//   minter: [ '0x993d74f0EE94F9521821A302337F6Acc2A99baB2' ],
//   pauser: [],
//   transfer: []
// }
// ✅ Successfully revoked our superpowers from the ERC-20 contract
